package com.stivayou.reference1819;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CountryLetterAdapter extends RecyclerView.Adapter {

    List<Object> countriesAndLetters = new ArrayList<>();

    final static int TYPE_LETTER = 0;
    final static int TYPE_COUNTRY = 1;


    public CountryLetterAdapter(JSONArray countriesJson ){

        String lastLetter = "IGIYFYUFYUFYU";

        for( int i = 0; i < countriesJson.length(); i++ ){

            try {
                JSONObject countryJson = countriesJson.getJSONObject(i);

                Country country = new Country( countryJson );

                if( !country.name.startsWith( lastLetter ) ){
                    lastLetter = String.valueOf(country.name.charAt(0));

                    Letter letter = new Letter();
                    letter.letter = lastLetter;
                    countriesAndLetters.add( letter );

                }

                countriesAndLetters.add( country );

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }

    @Override
    public int getItemViewType(int position) {

        if( countriesAndLetters.get(position) instanceof Letter ){
            return TYPE_LETTER;
        }

        return TYPE_COUNTRY;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if( viewType == TYPE_LETTER ){

            TextView tv = new TextView( parent.getContext() );

            return new LetterViewHolder( tv );

        }
        else{
            LayoutInflater inflater = LayoutInflater.from( parent.getContext() );

            View rowView = inflater.inflate( R.layout.json_row, parent, false );

            return new CountryViewHolder( rowView );
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if( getItemViewType(position) == TYPE_LETTER ){

            LetterViewHolder letterHolder = (LetterViewHolder) holder;

            String letter = ((Letter) countriesAndLetters.get(position)).letter;

            letterHolder.text.setText( letter );

        }
        else{
            CountryViewHolder countryHolder = (CountryViewHolder) holder;

            Country country = (Country) countriesAndLetters.get(position);

            countryHolder.tv1.setText( country.name );
            countryHolder.tv2.setText( country.code );
            countryHolder.tv3.setText( country.count + "" );

            if( position % 2 == 0 ){
                countryHolder.tv3.setBackgroundColor(Color.BLUE );
            } else {
                countryHolder.tv3.setBackgroundColor(Color.TRANSPARENT );
            }
        }

    }

    @Override
    public int getItemCount() {
        return countriesAndLetters.size();
    }

    public class CountryViewHolder extends RecyclerView.ViewHolder {

        TextView tv1, tv2, tv3;

        public CountryViewHolder(View itemView) {
            super(itemView);

            tv1 = itemView.findViewById( R.id.json_row_text );
            tv2 = itemView.findViewById( R.id.json_row_text2 );
            tv3 = itemView.findViewById( R.id.json_row_text3 );

        }
    }

    public class LetterViewHolder extends RecyclerView.ViewHolder {

        TextView text;

        public LetterViewHolder(View itemView) {
            super(itemView);

            text = (TextView) itemView;
        }
    }

}
