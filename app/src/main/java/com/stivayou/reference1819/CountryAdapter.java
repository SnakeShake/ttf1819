package com.stivayou.reference1819;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CountryAdapter extends RecyclerView.Adapter {

    //JSONArray countries;
    List<Country> countries = new ArrayList<>();

    public CountryAdapter( JSONArray countriesJson ){

        for( int i = 0; i < countriesJson.length(); i++ ){

            try {
                JSONObject countryJson = countriesJson.getJSONObject(i);

                Country country = new Country( countryJson );

                countries.add( country );

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from( parent.getContext() );

        View rowView = inflater.inflate( R.layout.json_row, parent, false );

        return new CountryViewHolder( rowView );
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        CountryViewHolder countryHolder = (CountryViewHolder) holder;

        Country country = countries.get(position);

        countryHolder.tv1.setText( country.name );
        countryHolder.tv2.setText( country.code );
        countryHolder.tv3.setText( country.count + "" );

        if( position % 2 == 0 ){
            countryHolder.tv3.setBackgroundColor(Color.BLUE );
        } else {
            countryHolder.tv3.setBackgroundColor(Color.TRANSPARENT );
        }

        /*try {

            JSONObject currentCountry = countries.getJSONObject(position);

            countryHolder.tv1.setText( currentCountry.getString("name") );
            countryHolder.tv2.setText( currentCountry.getString("code") );
            countryHolder.tv3.setText( currentCountry.getString("count") );

            if( position % 2 == 0 ){
                countryHolder.tv3.setBackgroundColor(Color.BLUE );
            } else {
                countryHolder.tv3.setBackgroundColor(Color.TRANSPARENT );
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }*/

    }

    @Override
    public int getItemCount() {
        return countries.size();
    }

    public class CountryViewHolder extends RecyclerView.ViewHolder {

        TextView tv1, tv2, tv3;

        public CountryViewHolder(View itemView) {
            super(itemView);

            tv1 = itemView.findViewById( R.id.json_row_text );
            tv2 = itemView.findViewById( R.id.json_row_text2 );
            tv3 = itemView.findViewById( R.id.json_row_text3 );

        }
    }

}
