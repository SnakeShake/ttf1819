package com.stivayou.reference1819;

import org.json.JSONException;
import org.json.JSONObject;

public class Country {

    public String name, code;
    public int locations, count;

    public Country(JSONObject o) throws JSONException {

        name = o.getString("name");
        code = o.getString("code");
        locations = o.getInt("locations");
        count = o.getInt("count");


    }

}
