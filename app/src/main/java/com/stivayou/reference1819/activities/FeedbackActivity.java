package com.stivayou.reference1819.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.stivayou.reference1819.R;

public class FeedbackActivity extends AppCompatActivity {

    EditText editName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        editName = findViewById(R.id.feedback_edit);

        findViewById(R.id.feedback_toast).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(FeedbackActivity.this, R.string.feedback_toast,Toast.LENGTH_SHORT).show();

            }
        });

        findViewById(R.id.feedback_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(FeedbackActivity.this);
                builder.setTitle( R.string.feedback_dialog_title )
                        .setMessage( R.string.feedback_dialog_message )
                        .setPositiveButton(R.string.common_yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Toast.makeText(FeedbackActivity.this, R.string.feedback_toast,Toast.LENGTH_SHORT).show();

                            }
                        })
                        .setNegativeButton( R.string.common_no, null );

                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });

        findViewById(R.id.feedback_validate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if( editName.getText().toString().equals("") ){
                    editName.setError( getResources().getString(R.string.feedback_error_empty) );
                }

            }
        });


    }
}
