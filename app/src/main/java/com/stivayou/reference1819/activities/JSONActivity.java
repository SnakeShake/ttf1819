package com.stivayou.reference1819.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.stivayou.reference1819.DataGetter;
import com.stivayou.reference1819.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONActivity extends AppCompatActivity {

    LinearLayout linear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json);

        linear = findViewById(R.id.json_linear);

        DataGetter getter = new DataGetter( "https://api.openaq.org/v1/countries" );
        getter.getData(new DataGetter.ResultCallback() {
            @Override
            public void onError(int errorCode) {
                Toast.makeText( JSONActivity.this, R.string.common_error_internet, Toast.LENGTH_LONG ).show();
            }

            @Override
            public void onSuccess(String result) {

                try {
                    JSONObject obj = new JSONObject(result);

                    JSONArray array = obj.getJSONArray( "results" );

                    for( int i = 0; i < array.length(); i++ ){

                        JSONObject rowObj = array.getJSONObject(i);

                        /*LinearLayout ll = new LinearLayout( JSONActivity.this );

                        TextView tv = new TextView( JSONActivity.this );
                        TextView tv2 = new TextView( JSONActivity.this );
                        TextView tv3 = new TextView( JSONActivity.this );*/

                        //tv.setText( array.get(i).toString() );

                        LayoutInflater inflater = LayoutInflater.from( JSONActivity.this );

                        LinearLayout ll = (LinearLayout) inflater.inflate( R.layout.json_row, linear, false );

                        ((TextView) ll.findViewById(R.id.json_row_text)).setText( rowObj.getString("name") );
                        ((TextView) ll.findViewById(R.id.json_row_text2)).setText( rowObj.getString("code") );
                        ((TextView) ll.findViewById(R.id.json_row_text3)).setText( rowObj.getString("locations") );

                        /*tv.setText( rowObj.getString("name") );
                        tv2.setText( rowObj.getString("code") );
                        tv3.setText( rowObj.getString("locations") );*/

                        //tv2.setBackgroundColor(Color.CYAN );

                        /*ll.addView( tv );
                        ll.addView( tv2 );
                        ll.addView( tv3 );*/

                        linear.addView(ll);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }
}
