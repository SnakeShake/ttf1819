package com.stivayou.reference1819.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.stivayou.reference1819.CountryAdapter;
import com.stivayou.reference1819.CountryLetterAdapter;
import com.stivayou.reference1819.DataGetter;
import com.stivayou.reference1819.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class InstanceActivity extends AppCompatActivity {

    private static final String SAVE_ID = "si_countries";
    private static final String SAVE_TIMESTAMP = "si_timestamp";
    private static final String PREFS_BASE = "yufgfrthdtyfdtykjlxlkjsdngkbsdkghjkdfg";


    RecyclerView recycler;
    //String countries = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // We'll re-use the previous layout
        setContentView(R.layout.activity_recycler);

        recycler = findViewById(R.id.recycler_recycler);
        recycler.setLayoutManager( new LinearLayoutManager( this ));

        /*if( savedInstanceState != null && savedInstanceState.containsKey( SAVE_ID ) ){

            String sArray = savedInstanceState.getString( SAVE_ID );

            countries = sArray;

            try {
                Toast.makeText( this, "From saved instance state", Toast.LENGTH_LONG ).show();
                recycler.setAdapter( new CountryAdapter( new JSONArray( sArray ) ) );
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        else{
            DataGetter getter = new DataGetter( "https://api.openaq.org/v1/countries" );
            getter.getData(new DataGetter.ResultCallback() {
                @Override
                public void onError(int errorCode) {
                    Toast.makeText( InstanceActivity.this, R.string.common_error_internet, Toast.LENGTH_LONG ).show();
                }

                @Override
                public void onSuccess(String result) {

                    try {
                        JSONObject obj = new JSONObject(result);

                        JSONArray array = obj.getJSONArray( "results" );

                        countries = array.toString();
                        Toast.makeText( InstanceActivity.this, "From internet", Toast.LENGTH_LONG ).show();
                        recycler.setAdapter( new CountryAdapter( array ) );

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }*/



    }

    @Override
    protected void onResume() {
        super.onResume();

        final SharedPreferences prefs = getSharedPreferences( PREFS_BASE, Context.MODE_PRIVATE );

        if( ( !prefs.contains(SAVE_ID) ) || (prefs.getLong( SAVE_TIMESTAMP, 0L ) < System.currentTimeMillis() - ( 60 * 1000 * 5 ) ) ){

            DataGetter getter = new DataGetter( "https://api.openaq.org/v1/countries" );
            getter.getData(new DataGetter.ResultCallback() {
                @Override
                public void onError(int errorCode) {
                    Toast.makeText( InstanceActivity.this, R.string.common_error_internet, Toast.LENGTH_LONG ).show();
                }

                @Override
                public void onSuccess(String result) {

                    try {
                        JSONObject obj = new JSONObject(result);

                        JSONArray array = obj.getJSONArray( "results" );

                        String countries = array.toString();
                        Toast.makeText( InstanceActivity.this, "From internet", Toast.LENGTH_LONG ).show();
                        //recycler.setAdapter( new CountryAdapter( array ) );
                        recycler.setAdapter( new CountryLetterAdapter( array ) );


                        SharedPreferences.Editor edit = prefs.edit();
                        edit.putString( SAVE_ID, countries );
                        edit.putLong(SAVE_TIMESTAMP, System.currentTimeMillis());
                        edit.apply();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

        }
        else{

            String countries = prefs.getString(SAVE_ID,"");
            Toast.makeText( InstanceActivity.this, "From prefs", Toast.LENGTH_LONG ).show();
            try {
                //recycler.setAdapter( new CountryAdapter( new JSONArray( countries ) ) );
                recycler.setAdapter( new CountryLetterAdapter( new JSONArray( countries ) ) );

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }

    /*@Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if( !countries.equals("") ){
            outState.putString(SAVE_ID,countries);
        }

    }*/
}
