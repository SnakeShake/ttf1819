package com.stivayou.reference1819.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.stivayou.reference1819.DataGetter;
import com.stivayou.reference1819.R;

public class InternetActivity extends AppCompatActivity {

    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internet);

        text = findViewById(R.id.internet_text);

        DataGetter getter = new DataGetter( "https://api.openaq.org/v1/countries" );
        getter.getData(new DataGetter.ResultCallback() {
            @Override
            public void onError(int errorCode) {
                Toast.makeText( InternetActivity.this, R.string.common_error_internet, Toast.LENGTH_LONG ).show();
            }

            @Override
            public void onSuccess(String result) {

                text.setText( result );

            }
        });

    }
}
