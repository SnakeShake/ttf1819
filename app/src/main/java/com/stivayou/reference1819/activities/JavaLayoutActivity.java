package com.stivayou.reference1819.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.stivayou.reference1819.R;

public class JavaLayoutActivity extends AppCompatActivity {

    LinearLayout linear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_java_layout);

        linear = findViewById(R.id.jlayout_linear);

        findViewById(R.id.jlayout_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TextView tv = new TextView( JavaLayoutActivity.this );

                tv.setText( R.string.jlayout_text );
                tv.setGravity(Gravity.CENTER_HORIZONTAL );

                linear.addView( tv );

            }
        });

        findViewById(R.id.jlayout_remove).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if( linear.getChildCount() > 0 )
                    linear.removeViewAt( linear.getChildCount() - 1 );

            }
        });
    }
}
