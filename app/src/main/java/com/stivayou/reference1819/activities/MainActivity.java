package com.stivayou.reference1819.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.stivayou.reference1819.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.main_button_qualifier).setOnClickListener( this );

        // Feedback button
        findViewById(R.id.main_button_feedback).setOnClickListener( this );

        // 9-patch button
        findViewById(R.id.main_button_patch).setOnClickListener( this );

        // Selector button
        findViewById(R.id.main_button_selector).setOnClickListener( this );

        // Constraint button
        findViewById(R.id.main_button_constraint).setOnClickListener( this );

        // Internet & Async button
        findViewById(R.id.main_button_internet).setOnClickListener( this );

        // Java Layout button
        findViewById(R.id.main_button_jlayout).setOnClickListener( this );

        // JSON button
        findViewById(R.id.main_button_json).setOnClickListener( this );

        // Recycler button
        findViewById(R.id.main_button_recycler).setOnClickListener( this );

        // Instance States button
        findViewById(R.id.main_button_instance).setOnClickListener( this );

    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle( R.string.main_dialog_title )
                .setMessage( R.string.main_dialog_message )
                .setPositiveButton(R.string.common_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MainActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton( R.string.common_cancel, null );

        builder.create().show();
        
    }

    @Override
    public void onClick(View view) {

        switch ( view.getId() ){
            case R.id.main_button_qualifier:
                startActivity( new Intent(this,QualifierActivity.class) );
                break;

            case R.id.main_button_feedback:
                startActivity( new Intent(this,FeedbackActivity.class) );
                break;

            case R.id.main_button_patch:
                startActivity( new Intent(this,PatchActivity.class) );
                break;

            case R.id.main_button_selector:
                startActivity( new Intent(this,SelectorActivity.class) );
                break;

            case R.id.main_button_constraint:
                startActivity( new Intent(this,ConstraintActivity.class) );
                break;

            case R.id.main_button_internet:
                startActivity( new Intent(this,InternetActivity.class) );
                break;

            case R.id.main_button_jlayout:
                startActivity( new Intent(this,JavaLayoutActivity.class) );
                break;

            case R.id.main_button_json:
                startActivity( new Intent(this,JSONActivity.class) );
                break;

            case R.id.main_button_recycler:
                startActivity( new Intent(this,RecyclerActivity.class) );
                break;

            case R.id.main_button_instance:
                startActivity( new Intent(this,InstanceActivity.class) );
                break;
        }

    }
}
