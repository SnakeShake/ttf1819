package com.stivayou.reference1819.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.stivayou.reference1819.R;

public class ConstraintActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_constraint);

        findViewById(R.id.buttonLeft).setOnClickListener( new SwitchClickListener((TextView) findViewById(R.id.textLeft)) );
        findViewById(R.id.buttonRight).setOnClickListener( new SwitchClickListener((TextView) findViewById(R.id.textRight)) );
    }

    private class SwitchClickListener implements View.OnClickListener {

        TextView text;

        SwitchClickListener(TextView text) {
            this.text = text;
        }

        @Override
        public void onClick(View view) {

            if( text.getText().equals( getResources().getString( R.string.constraint_text_three ) ) ){
                text.setText( R.string.constraint_text_six );
                ((Button) view).setText( R.string.constraint_button_label_less );
            }
            else{
                text.setText( R.string.constraint_text_three );
                ((Button) view).setText( R.string.constraint_button_label_more );
            }

        }
    }
}
