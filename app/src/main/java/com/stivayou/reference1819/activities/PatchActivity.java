package com.stivayou.reference1819.activities;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.stivayou.reference1819.R;

public class PatchActivity extends AppCompatActivity {

    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patch);

        text = findViewById(R.id.patch_text);

        findViewById(R.id.patch_button_gradient).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                text.setBackgroundResource( R.drawable.main_background );
                text.setTextColor( Color.BLACK );
                text.setPadding(0,0,0,0);

            }
        });

        findViewById(R.id.patch_button_flat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                text.setBackgroundResource( R.drawable.earthrise_flat );
                text.setTextColor( Color.WHITE );
                text.setPadding(0,0,0,0);

            }
        });

        findViewById(R.id.patch_button_patch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                text.setBackgroundResource( R.drawable.earthrise );
                text.setTextColor( Color.WHITE );

            }
        });

    }
}
